## Controladors

Els controladors són classes que s'utilitzen per donar resposta a les
sol·licituds dels usuaris.

Si la resposta a una determinada ruta és molt senzilla es pot implementar
directament en el fitxer *routes.php* com una *clousure*. Habitualment, però,
donar una resposta implica recuperar models de la base de dades i realitzar
una sèrie de comprovacions.

A cada controlador s'hi agrupen peticions relacionades entre elles.
Habitualment, un controlador dóna resposta a totes les peticions relacionades
amb un cert model.

Els controladors es guarden al directori `app/Http/Controllers` i han
de derivar de la classe *Controller*.

Un exemple de controlador senzill podria ser el següent:

```php5
<?php

namespace App\Http\Controllers;

use App\Garden;
use App\Http\Controllers\Controller;

class GardenController extends Controller
{
  public function handleView(Garden $garden) {
    return view('garden.view', ['garden'=>$garden]);
  }
}
```

Al fitxer *routes.php* podríem adreçar la petició al controlador d'aquesta
manera:

```php5
Route::get('garden/view/{garden}', 'GardenController@handleView');
```

Noteu com hem utilitzat el nom del model per tal de rebre directament un
jardí en comptes de rebre el seu *id*.

### Middleware

Imaginem ara que només l'usuari propietari d'un jardí el pot veure. No
només això, sinó que el controlador de jardí té diverses operacions i
volem que un usuari no pugui accedir a cap d'elles si no és el
propietari del jardí.

Podríem repetir el codi de comprovació a cada mètode. O, una mica millor,
podríem crear un mètode que fes cada comprovació i repetir la crida a
aquest mètode des de tots els mètodes que resolen alguna acció.

És més pràctic, però, indicar al controlador quines restriccions s'han
d'aplicar, i que ja es faci automàticament cada cop que una acció es
dirigeixi al controlador en qüestió.

Això ho podem fer en el constructor del controlador:

```php5
public function __construct()
{
  $this->middleware('auth');
}
```

Alternativament, ho podem fer al mateix *routes.php*:

```php5
Route::get('garden/view/{garden}', [
  'middleware' => 'auth',
  'uses' => 'GardenController@handleView'
]);
```

El middleware *auth* està definit pel propi framework i evita que un usuari
que no s'hagi autenticat pugui accedir a una certa ruta.

Nosaltres podem escriure el nostre propi middleware. Ho farem al directori
`app/Http/Middleware`. Podem demanar-li a l'*artisan* que ens creï l'esquelet
per nosaltres:

```
php artisan make:middleware OwnedGardenMiddleware
```

Editem després el fitxer obtingut amb el nostre codi:

```php5
<?php

namespace App\Http\Middleware;

use Closure;

class OwnedGardenMiddleware
{
  /**
   * Handle an incoming request.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \Closure  $next
   * @return mixed
   */
  public function handle($request, Closure $next) {
    $garden = $request->garden;
    if ($garden->isOwner() === false) {
        return Redirect::to('/')->with('messages', array(
                array('type' => 'danger', 'text' => 'You don\'t own this garden!')
        ));
    }
    return $next($request);
  }
}
```

En aquest codi hem afegit el tercer paràmetre al mètode *handle()*. La idea és
que aquest paràmetre el rebrem a través de la ruta utilitzada. El mètode
*isOwner()* de *Garden* agafa l'usuari actual i comprova si coincideix amb el
propietari del jardí.

Si no ho és es redirigeix a l'arrel de la web. Si ho és es passa la
sol·licitud al següent filtre, o al punt de resolució final si no hi ha més
filtres.

El primer paràmetre, `$request` es pot utilitzar per obtenir les dades que
s'han enviat amb la sol·licitud, amb el mètode `input()`.

Per poder utilitzar aquest middleware l'hem de registrar al fitxer
`app/Http/Kernel.php`:

```php5
protected $routeMiddleware = [
  'auth' => \App\Http\Middleware\Authenticate::class,
  'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
  'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
  'throttle' => \Illuminate\Routing\Middleware\ThrottleRequests::class,
	'garden.owned' => \App\Http\Middleware\OwnedGardenMiddleware::class,
];
```

Hem afegit l'última línia redirigint el nom `garden.owner` a la classe que
acabem de fer.

Ara, podem afegir aquest filtre al filtre que ja teníem:

```php5
Route::get('garden/view/{garden}', [
  'middleware' => ['auth','garden.owned'],
  'uses' => 'GardenController@handleView'
]);
```

Un cop feta aquesta feina és molt senzill reutilitzar aquest filtre per totes
les rutes que impliquin que s'ha de ser el propietari d'un jardí per poder-hi
accedir.
