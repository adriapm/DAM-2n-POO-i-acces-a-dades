package examenJDBC;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DB {

	private static String url = "jdbc:mysql://localhost:3306/sakila";
	private static String user = "root";
	private static String passwd = "super3";
	
	private static Connection con = null;
	
	public static Connection connection() throws SQLException {
		if (con==null) {
			con = DriverManager.getConnection(url, user, passwd);
		}
		return con;
	}
	
	public static void closeConnection() throws SQLException {
		if (con!=null) {
			try {
				con.close();
			} finally {
				con = null;
			}
		}
	}
	
	private DB() {}
	
	public static boolean insertRental(Customer customer, Inventory inventory, Staff staff) throws SQLException {
		boolean ok = true;
		Film film = inventory.film();
		
		connection().setAutoCommit(false);
		try (
			PreparedStatement stRental = connection().prepareStatement(
					"insert into rental(rental_date, inventory_id, customer_id, staff_id) values(now(), ?, ?, ?)",
					Statement.RETURN_GENERATED_KEYS);
			PreparedStatement stPayment = connection().prepareStatement(
					"insert into payment(customer_id, staff_id, rental_id, amount, payment_date) values(?, ?, ?, ?, now())");
		) {
			// Faltaria comprovar que l'ítem no estigui ja llogat
			stRental.setInt(1, inventory.id);
			stRental.setInt(2, customer.id);
			stRental.setInt(3, staff.id);
			stRental.executeUpdate();
			ResultSet rs = stRental.getGeneratedKeys();
			rs.next();
			int rentalId = rs.getInt(1);
			rs.close();
		
			stPayment.setInt(1, customer.id);
			stPayment.setInt(2, staff.id);
			stPayment.setInt(3, rentalId);
			stPayment.setDouble(4, film.rentalRate);
			stPayment.executeUpdate();
		} catch (SQLException e) {
			connection().rollback();
			throw e;
		} finally {
			connection().setAutoCommit(true);
		}
		
		return ok;
	}
	
	
}
